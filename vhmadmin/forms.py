from django import forms
from django.core.exceptions import ValidationError
from django.forms.forms import Form
from django.forms.models import ModelForm, ModelChoiceField
from django.utils.translation import ugettext, ugettext_lazy as _
from vhmdb.models import username_validator
from vhmdb.models.rent import VhostRent
from vhmdb.models.sitepackage import SitePackage
from vhmdb.models.user import User, UserGroup
from vhmdb.models.vhost import Vhost, Interface
from vhmweb.fields import DateTimeField
from vhmweb.forms import ClassMixin
from vhmweb.forms import RentForm as OrRentForm
from vhmweb import settings


class RentForm(OrRentForm):

    user = forms.ModelChoiceField(queryset=User.objects, label=_("User"), empty_label=None)

    class Meta(OrRentForm.Meta):
        widgets = {}
        labels = {
            "status": _("Status"),
        }

    def clean(self):

        data = super(OrRentForm, self).clean()

        return data


class RentGroupForm(Form, ClassMixin):

    group = ModelChoiceField(queryset=UserGroup.objects, label=_("Group"), empty_label=None)

    start_date = DateTimeField(label=_("Start date"))
    end_date = DateTimeField(label=_("End date"))

    package = forms.ModelChoiceField(queryset=SitePackage.objects, label=_("Package"),
                                     empty_label=_("No package"), required=False)

    def clean(self):

        data = super(RentGroupForm, self).clean()

        group = data.get("group", None)
        start_date = data.get("start_date", None)
        end_date = data.get("end_date", None)
        package = data.get("package", None)

        if not start_date or not end_date:
            return data

        vhost = Vhost.get_available_at(start_date, end_date)

        if group:
            for u in group.users.all():
                r = VhostRent()
                r.vhost = vhost
                r.user = u
                r.start_date = start_date
                r.end_date = end_date
                r.package = package
                r.full_clean()
                del r

        return data


class UserForm(ModelForm, ClassMixin):

    widget_class = "form-control input-sm"

    class Meta:
        model = User
        fields = (
            "name",
            "display_name",
            "password", "password_confirm",
            "max_vhosts",
            "admin",
            "disabled",
        )
        labels = {
            "name": _("Username"),
            "display_name": _("Display name"),
            "max_vhosts": _("VirtualHost rent limit"),
            "admin": _("Administrator"),
            "disabled": _("Disabled"),
        }
        help_texts = {
            "name": _("Should only contain lowercase letters, digits and underscores. "
                      "Cannot be changed after the user is created."),
            "display_name": _("This name can contain any types of letters. Users can change their own display names."),
            "max_vhosts": _("Enter -1 to use the default limit."),
            "disabled": _("The user cannot log in to VhostManager if it is disabled."),
        }

    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput)
    password_confirm = forms.CharField(label=_("Retype password"), widget=forms.PasswordInput)

    def clean(self):

        data = super(UserForm, self).clean()

        password = data.get("password", None)
        passwordc = data.get("password_confirm", None)
        if not password:
            return data

        if not passwordc or password != passwordc:
            raise ValidationError(_("Passwords do not match."))

        return data

    def save(self, commit=True):

        user = self.instance
        user.set_password(user.password)

        return super(UserForm, self).save(commit=commit)


class UserEditForm(ModelForm, ClassMixin):

    widget_class = "form-control input-sm"

    class Meta(UserForm.Meta):
        fields = (
            "display_name",
            "max_vhosts",
            "admin",
            "disabled",
        )


class UserPasswordForm(Form, ClassMixin):

    password = forms.CharField(label=_("New password"), widget=forms.PasswordInput)
    password_confirm = forms.CharField(label=_("Retype new password"), widget=forms.PasswordInput)

    def clean(self):

        data = super(UserPasswordForm, self).clean()

        password = data.get("password", None)
        passwordc = data.get("password_confirm", None)
        if not password:
            return data

        if not passwordc or password != passwordc:
            raise ValidationError(ugettext("Passwords do not match."))

        return data


class UserGroupForm(Form, ClassMixin):

    name_list = forms.CharField(widget=forms.Textarea, label=_("Name list"),
                                help_text=_("One name per line. The name will be set "
                                                        "as the user's display name, and the username will be "
                                                        "a converted version of it."))

    password = forms.CharField(widget=forms.PasswordInput, label=_("Password"),
                               help_text=_("The same password will be used for all created users. "
                                                       "To make unique passwords, generate passwords for the group."))
    password_confirm = forms.CharField(widget=forms.PasswordInput, label=_("Retype password"))

    max_vhosts = forms.IntegerField(label=_("VirtualHost rent limit"), initial=-1,
                                    help_text=_("Enter -1 to use the default limit."))

    admin = forms.BooleanField(label=_("Administrator"), initial=False, required=False)
    disabled = forms.BooleanField(label=_("Disabled"), initial=False, required=False,
                                  help_text=_("The user cannot log in to VhostManager if it is disabled."))

    group = forms.CharField(label=_("Group"),
                            help_text=_("If you want to add all created users to a group, "
                                                    "enter the group name here. The group will be created "
                                                    "if it does not exist"), required=False)

    def clean(self):

        data = super(UserGroupForm, self).clean()

        password = data.get("password", None)
        passwordc = data.get("password_confirm", None)
        if not password:
            return data

        if not passwordc or password != passwordc:
            raise ValidationError(ugettext("Passwords do not match."))

        group = data.get("group", None)
        if group:
            try:
                username_validator(group)
            except ValidationError:
                raise ValidationError(ugettext("Illegal characters in group name. "
                                               "Valid characters are a-z, 0-10, and underscore (_)."))

        return data


class GroupForm(ModelForm):

    class Meta:
        model = UserGroup
        fields = ("name", "users")

    name = forms.CharField(max_length=255, widget=forms.TextInput(attrs={"class": "form-control input-sm"}),
                           label=_("Name"))

    users = forms.ModelMultipleChoiceField(widget=forms.SelectMultiple(attrs={"class": "form-control"}),
                                           queryset=User.objects, label=_("Members"), required=False)


class PasswordGenerateForm(Form, ClassMixin):

    length = forms.IntegerField(label=_("Password length"), min_value=1, initial=6)

    use_uppercase = forms.BooleanField(label=_("Use uppercase letters"), initial=True, required=False)
    use_digits = forms.BooleanField(label=_("Use digits"), initial=True, required=False)

    for_admins = forms.BooleanField(label=_("Generate for admins as well"), initial=False, required=False)


class VhostForm(ModelForm, ClassMixin):

    class Meta:
        model = Vhost
        fields = ("name", "interface", "port", "options", "allow_override")
        labels = {
            "name": _("Name"),
            "port": _("Port"),
            "options": _("Options"),
            "allow_override": _("Override setting"),
        }
        help_texts = {
            "options": _("Options -setting for Apache VirtualHost configuration."),
            "allow_override": _("AllowOverride -setting for Apache VirtualHost configuration.")
        }
        widgets = {
            "options": forms.TextInput(),
            "allow_override": forms.TextInput(),
        }

    interface = forms.ModelChoiceField(queryset=Interface.objects, label=_("Network interface"),
                                       empty_label=_("Default"), required=False)


class VhostGroupForm(Form, ClassMixin):

    prefix = forms.CharField(label=_("Prefix"), validators=[username_validator],
                             help_text=_("This is put before the VirtualHost's number in the VirtualHost's name."))

    count = forms.IntegerField(label=_("VirtualHost count"), min_value=1)

    port_start = forms.IntegerField(label=_("Port start"), min_value=1, max_value=65535,
                                    help_text=_("The start number of VirtualHost ports. The first VirtualHost will "
                                                "have port start + 1"))

    options = forms.CharField(label=_("Options"), initial="All",
                              help_text=_("Options -setting for Apache VirtualHost configuration."))
    allow_override = forms.CharField(label=_("Override setting"), initial="All",
                                     help_text=_("AllowOverride -setting for Apache VirtualHost configuration."))


class SitePackageForm(ModelForm, ClassMixin):

    class Meta:
        model = SitePackage
        fields = ("name", "description", "package")
        labels = {
            "name": _("Name"),
            "description": _("Description"),
            "package": _("Package file"),
        }
        help_texts = {
            "package": _("NOTE: The package file must be a ZIP-file. RAR-files are not supported.")
        }


class SitePackageEditForm(SitePackageForm):

    class Meta(SitePackageForm.Meta):
        fields = ("name", "description")


class SitePackageUpdateForm(SitePackageForm):

    class Meta(SitePackageForm.Meta):
        fields = ("package",)
        labels = {
            "package": _("New package"),
        }
        widgets = {
            "package": forms.FileInput,
        }