from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import transaction
from django.http.response import HttpResponseRedirect, HttpResponseNotFound
from django.utils.translation import ugettext_lazy as _, ugettext
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.views.generic.list import ListView
from vhmadmin.forms import GroupForm, PasswordGenerateForm
from vhmadmin.utils import random_text
from vhmadmin.views import AdminMixin
from vhmdb.models.user import UserGroup
from vhmweb.requests import get_request
from vhmweb.views import TitleMixin, ListNameMixin, SubmitBtnMixin, MessageMixin


__all__ = (
    "GroupList",
    "GroupDetail",
    "GroupPasswordGenerate",
    "GroupPasswordGenerateResult",
    "GroupCreate",
    "GroupEdit",
    "GroupDelete",
)


class GroupList(ListView, TitleMixin, ListNameMixin, AdminMixin):

    title = _("Groups")
    template_name = "vhmadmin/group/list.html"

    list_name = "groups"

    def get_queryset(self):
        return UserGroup.objects.all()


class GroupDetail(DetailView, TitleMixin, AdminMixin):

    template_name = "vhmadmin/group/detail.html"

    model = UserGroup
    context_object_name = "group"
    slug_field = "pk"

    def get_title(self):
        group = self.get_object(self.get_queryset())
        return ugettext("Members of %s") % group.name


class GroupPasswordGenerate(FormView, TitleMixin, SubmitBtnMixin, AdminMixin):

    template_name = "vhmadmin/base_form.html"

    form_class = PasswordGenerateForm

    submit_btn_text = _("Generate")
    submit_btn_type = "success"

    def __init__(self, *args, **kwargs):
        super(GroupPasswordGenerate, self).__init__(*args, **kwargs)
        self.group = None

    def get_success_url(self):
        return reverse("vhmadmin_group_pwgen_result")

    def get_title(self):
        return ugettext("Generate passwords for group %s") % self.group.name

    def dispatch(self, request, *args, **kwargs):

        slug = kwargs.pop("slug", None)
        if not slug:
            return HttpResponseNotFound()
        try:
            self.group = UserGroup.objects.get(pk=slug)
        except UserGroup.objects.DoesNotExist:
            return HttpResponseNotFound()

        return super(GroupPasswordGenerate, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):

        data = form.cleaned_data

        group = self.group
        length = data["length"]
        uppercase = data["use_uppercase"]
        digits = data["use_digits"]
        admins = data["for_admins"]
        results = {}

        with transaction.commit_on_success():
            for u in group.users.all():

                if not admins and u.admin:
                    continue

                pwd = random_text(length, uppercase=uppercase, digits=digits)
                u.set_password(pwd)
                u.save()

                results[u.name] = pwd

        get_request().session["pwgen_result"] = results

        return super(GroupPasswordGenerate, self).form_valid(form)


class GroupPasswordGenerateResult(TemplateView, TitleMixin, AdminMixin):

    title = _("Password generation results")
    template_name = "vhmadmin/group/pwgen_result.html"

    def __init__(self, *args, **kwargs):
        super(GroupPasswordGenerateResult, self).__init__(*args, **kwargs)
        self.results = None

    def dispatch(self, request, *args, **kwargs):

        result_data = request.session.get("pwgen_result", None)
        if not result_data:
            return HttpResponseRedirect(reverse("vhmadmin_index"))
        results = ""
        for name, pw in result_data.iteritems():
            results += "%s: %s\n" % (name, pw)
        self.results = results

        resp = super(GroupPasswordGenerateResult, self).dispatch(request, *args, **kwargs)

        del request.session["pwgen_result"]

        return resp

    def get_context_data(self, **kwargs):
        data = super(GroupPasswordGenerateResult, self).get_context_data(**kwargs)
        data["pwgen_results"] = self.results
        return data


class GroupCreate(CreateView, TitleMixin, SubmitBtnMixin, MessageMixin, AdminMixin):

    title = _("New group")
    template_name = "vhmadmin/base_form.html"

    form_class = GroupForm

    submit_btn_text = _("Create")
    submit_btn_type = "success"

    message = _("Group created.")
    message_type = messages.SUCCESS

    def get_success_url(self):
        return reverse("vhmadmin_group_list")


class GroupEdit(UpdateView, TitleMixin, SubmitBtnMixin, MessageMixin, AdminMixin):

    template_name = "vhmadmin/base_form.html"

    model = UserGroup
    form_class = GroupForm
    slug_field = "pk"

    submit_btn_text = _("Save")
    submit_btn_type = "success"

    message = _("Group modified.")
    message_type = messages.SUCCESS

    def get_title(self):
        group = self.get_object(self.get_queryset())
        return "%s: %s" % (ugettext("Edit group"), group.name)

    def get_success_url(self):
        return reverse("vhmadmin_group_list")


class GroupDelete(DeleteView, TitleMixin, AdminMixin):

    template_name = "vhmadmin/group/delete.html"

    model = UserGroup
    slug_field = "pk"

    def get_title(self):
        group = self.get_object(self.get_queryset())
        return "%s: %s" % (ugettext("Delete group"), group.name)

    def get_success_url(self):
        return reverse("vhmadmin_group_list")

    def delete(self, request, *args, **kwargs):
        response = super(GroupDelete, self).delete(request, *args, **kwargs)
        if isinstance(response, HttpResponseRedirect):
            messages.add_message(request, messages.SUCCESS, ugettext("Group deleted."))
        return response