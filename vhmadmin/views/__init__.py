from django.http.response import HttpResponseForbidden
from django.utils.translation import ugettext_lazy as _
from django.views.generic.base import TemplateView
from vhmweb.views import TitleMixin, LoginMixin


class AdminMixin(LoginMixin):

    def dispatch(self, request, *args, **kwargs):

        if request.user and not request.user.admin:
            return HttpResponseForbidden()

        return super(AdminMixin, self).dispatch(request, *args, **kwargs)


class Index(TemplateView, TitleMixin, AdminMixin):

    title = _("Administration")

    template_name = "vhmadmin/index.html"


from vhmadmin.views.user import *
from vhmadmin.views.group import *
from vhmadmin.views.rent import *
from vhmadmin.views.vhost import *
from vhmadmin.views.sitepackage import *