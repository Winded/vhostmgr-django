from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.utils.translation import ugettext_lazy as _, ugettext
from vhmadmin.forms import SitePackageForm, SitePackageEditForm, SitePackageUpdateForm
from vhmadmin.views import AdminMixin
from vhmdb.models.sitepackage import SitePackage
from vhmweb.views import TitleMixin, ListNameMixin, SubmitBtnMixin, MessageMixin


class SitePackageList(ListView, TitleMixin, ListNameMixin, AdminMixin):

    title = _("Site packages")
    template_name = "vhmadmin/sitepackage/list.html"

    list_name = "packages"

    def get_queryset(self):
        return SitePackage.objects.all()


class SitePackageCreate(CreateView, TitleMixin, SubmitBtnMixin, MessageMixin, AdminMixin):

    title = _("New site package")
    template_name = "vhmadmin/base_form.html"

    form_class = SitePackageForm

    submit_btn_text = _("Upload")
    submit_btn_type = "success"

    message = _("Site package uploaded.")
    message_type = messages.SUCCESS

    def get_success_url(self):
        return reverse("vhmadmin_package_list")


class SitePackageEdit(UpdateView, TitleMixin, SubmitBtnMixin, MessageMixin, AdminMixin):

    template_name = "vhmadmin/base_form.html"

    model = SitePackage
    form_class = SitePackageEditForm
    slug_field = "pk"

    submit_btn_text = _("Save")
    submit_btn_type = "success"

    message = _("Site package modified.")
    message_type = messages.SUCCESS

    def get_title(self):
        package = self.get_object(self.get_queryset())
        return "%s: %s" % (ugettext("Edit site package"), package.name)

    def get_success_url(self):
        return reverse("vhmadmin_package_list")


class SitePackageUpdate(UpdateView, TitleMixin, SubmitBtnMixin, MessageMixin, AdminMixin):

    template_name = "vhmadmin/base_form.html"

    model = SitePackage
    form_class = SitePackageUpdateForm
    slug_field = "pk"

    submit_btn_text = _("Upload")
    submit_btn_type = "success"

    message = _("Site package updated.")
    message_type = messages.SUCCESS

    def get_title(self):
        package = self.get_object(self.get_queryset())
        return "%s: %s" % (ugettext("Update site package"), package.name)

    def get_success_url(self):
        return reverse("vhmadmin_package_list")


class SitePackageDelete(DeleteView, TitleMixin, AdminMixin):

    template_name = "vhmadmin/sitepackage/delete.html"

    model = SitePackage
    slug_field = "pk"

    def get_title(self):
        package = self.get_object(self.get_queryset())
        return "%s: %s" % (ugettext("Delete site package"), package.name)

    def get_success_url(self):
        return reverse("vhmadmin_package_list")

    def delete(self, request, *args, **kwargs):
        response = super(SitePackageDelete, self).delete(request, *args, **kwargs)
        if isinstance(response, HttpResponseRedirect):
            messages.add_message(request, messages.SUCCESS, ugettext("Site package deleted."))
        return response