from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import transaction
from django.http.response import HttpResponseRedirect, HttpResponseNotFound
from django.utils.translation import ugettext_lazy as _, ugettext
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.views.generic.list import ListView
from vhmadmin.forms import UserForm, UserEditForm, UserGroupForm, UserPasswordForm
from vhmadmin.utils import create_namelist
from vhmadmin.views import AdminMixin
from vhmdb.models.rent import VhostRent
from vhmdb.models.user import User, UserGroup
from vhmweb.requests import get_request
from vhmweb.views import TitleMixin, ListNameMixin, SubmitBtnMixin, MessageMixin


__all__ = (
    "UserList",
    "UserDetail",
    "UserCreate",
    "UserGroupCreate",
    "UserEdit",
    "UserEditPassword",
    "UserDelete"
)


class UserList(ListView, TitleMixin, ListNameMixin, AdminMixin):

    title = _("Users")
    template_name = "vhmadmin/user/list.html"

    list_name = "users"

    def get_queryset(self):

        request = get_request()

        q = User.objects.all()

        filter_admin = request.GET.get("filter_admin", None)
        filter_disabled = request.GET.get("filter_disabled", None)

        if filter_admin:
            if filter_admin == "1":
                q = q.filter(admin=True)
            else:
                q = q.filter(admin=False)

        if filter_disabled:
            if filter_disabled == "1":
                q = q.filter(disabled=True)
            else:
                q = q.filter(disabled=False)

        return q.order_by("name")


class UserDetail(DetailView, TitleMixin, AdminMixin):

    template_name = "vhmadmin/user/detail.html"

    model = User
    context_object_name = "user_obj"
    slug_field = "pk"

    def get_title(self):
        user = self.get_object(self.get_queryset())
        return "%s: %s" % (ugettext("User"), user.display_name)

    def get_context_data(self, **kwargs):
        data = super(UserDetail, self).get_context_data(**kwargs)

        user = data[self.context_object_name]
        data["rents"] = VhostRent.objects.filter(user=user).order_by("-pk")
        data["rents_active"] = VhostRent.objects.filter(user=user, status=VhostRent.STATUS_CURRENT).count()

        return data


class UserCreate(CreateView, TitleMixin, SubmitBtnMixin, MessageMixin, AdminMixin):

    title = _("Create user")
    template_name = "vhmadmin/base_form.html"

    form_class = UserForm

    submit_btn_text = _("Create")
    submit_btn_type = "success"

    message = _("User created.")
    message_type = messages.SUCCESS

    def get_success_url(self):
        return reverse("vhmadmin_user_list")


class UserGroupCreate(MessageMixin, TitleMixin, SubmitBtnMixin, AdminMixin):

    title = _("Create users from namelist")
    template_name = "vhmadmin/base_form.html"

    form_class = UserGroupForm

    submit_btn_text = _("Create users")
    submit_btn_type = "success"

    message = _("Users created.")
    message_type = messages.SUCCESS

    def get_success_url(self):
        return reverse("vhmadmin_user_list")

    def form_valid(self, form):

        data = form.cleaned_data

        if data["group"]:
            group = UserGroup.objects.filter(name=data["group"])
            if group.exists():
                group = group[0]
            else:
                group = UserGroup()
                group.name = data["group"]
                group.save()
        else:
            group = None

        namelist = create_namelist(data["name_list"])

        with transaction.commit_on_success():
            for name in namelist:

                u = User()
                u.name = name[0]
                u.display_name = name[1]
                u.set_password(data["password"])
                u.max_vhosts = data["max_vhosts"]
                u.admin = data["admin"]
                u.disabled = data["disabled"]
                u.full_clean()
                u.save()

                if group:
                    group.users.add(u)

        return super(UserGroupCreate, self).form_valid(form)


class UserEdit(UpdateView, TitleMixin, SubmitBtnMixin, MessageMixin, AdminMixin):

    template_name = "vhmadmin/base_form.html"

    model = User
    form_class = UserEditForm
    slug_field = "pk"

    submit_btn_text = _("Save")
    submit_btn_type = "success"

    message = _("User modified.")
    message_type = messages.SUCCESS

    def get_title(self):
        user = self.get_object(self.get_queryset())
        return "%s: %s" % (ugettext("Edit user"), user.display_name)

    def get_success_url(self):
        return reverse("vhmadmin_user_list")


class UserEditPassword(MessageMixin, TitleMixin, SubmitBtnMixin, AdminMixin):

    template_name = "vhmadmin/base_form.html"

    form_class = UserPasswordForm

    submit_btn_text = _("Save")
    submit_btn_type = "success"

    message = _("Password changed.")
    message_type = messages.SUCCESS

    def __init__(self, *args, **kwargs):
        super(UserEditPassword, self).__init__(*args, **kwargs)
        self.user = None

    def dispatch(self, request, *args, **kwargs):

        slug = kwargs.pop("slug", None)
        if not slug:
            return HttpResponseNotFound()
        try:
            self.user = User.objects.get(pk=slug)
        except User.objects.DoesNotExist:
            return HttpResponseNotFound()

        return super(UserEditPassword, self).dispatch(request, *args, **kwargs)

    def get_title(self):
        user = self.user
        return ugettext("Change %s's password.") % user.display_name

    def get_success_url(self):
        return reverse("vhmadmin_user_list")

    def form_valid(self, form):

        data = form.cleaned_data
        user = self.user

        user.set_password(data["password"])
        user.save()

        return super(UserEditPassword, self).form_valid(form)


class UserDelete(DeleteView, TitleMixin, AdminMixin):

    template_name = "vhmadmin/user/delete.html"

    model = User
    slug_field = "pk"

    def get_title(self):
        user = self.get_object(self.get_queryset())
        return "%s: %s" % (ugettext("Delete user"), user.display_name)

    def get_success_url(self):
        return reverse("vhmadmin_user_list")

    def delete(self, request, *args, **kwargs):
        response = super(UserDelete, self).delete(request, *args, **kwargs)
        if isinstance(response, HttpResponseRedirect):
            messages.add_message(request, messages.SUCCESS, ugettext("User deleted."))
        return response