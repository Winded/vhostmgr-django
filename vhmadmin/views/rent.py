from copy import deepcopy
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import transaction
from django.http.response import HttpResponseRedirect, HttpResponseNotFound
from django.utils.translation import ugettext_lazy as _, ugettext
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from vhmadmin.forms import RentForm, RentGroupForm
from vhmadmin.views import AdminMixin
from vhmdb.models.rent import VhostRent
from vhmdb.models.vhost import Vhost
from vhmweb.requests import get_request
from vhmweb.views import TitleMixin, ListNameMixin, SubmitBtnMixin, MessageMixin


__all__ = (
    "RentList",
    "RentCreate",
    "RentGroupCreate",
    "RentEdit",
    "RentCancel",
    "RentAccept",
    "RentReject",
    "RentDelete",
)


class RentList(ListView, TitleMixin, ListNameMixin, AdminMixin):

    title = _("Rents")
    template_name = "vhmadmin/rent/list.html"

    list_name = "rents"

    def get_queryset(self):

        request = get_request()

        q = VhostRent.objects.all()

        status = request.GET.get("status", None)
        if status:
            q = q.filter(status=status)

        return q.order_by("-pk")

    def get_context_data(self, **kwargs):
        data = super(RentList, self).get_context_data(**kwargs)

        status_filters = deepcopy(VhostRent.STATUS_CHOICES)
        data["status_filters"] = status_filters

        return data


class RentCreate(CreateView, TitleMixin, SubmitBtnMixin, MessageMixin, AdminMixin):

    title = _("New rent")
    template_name = "vhmadmin/base_form.html"

    form_class = RentForm

    submit_btn_text = _("Rent")
    submit_btn_type = "success"

    message = _("Rent created.")
    message_type = messages.SUCCESS

    def get_success_url(self):
        return reverse("vhmadmin_rent_list")


class RentGroupCreate(MessageMixin, TitleMixin, SubmitBtnMixin, AdminMixin):

    title = _("Rent for group")
    template_name = "vhmadmin/base_form.html"

    form_class = RentGroupForm

    submit_btn_text = _("Rent")
    submit_btn_type = "success"

    message = _("Rents created.")
    message_type = messages.SUCCESS

    def get_success_url(self):
        return reverse("vhmadmin_rent_list")

    def form_valid(self, form):

        data = form.cleaned_data

        with transaction.commit_on_success():
            for u in data["group"].users.all():
                r = VhostRent()
                r.vhost = Vhost.get_available_at(data["start_date"], data["end_date"])
                r.user = u
                r.start_date = data["start_date"]
                r.end_date = data["end_date"]
                r.package = data["package"]
                r.status = VhostRent.WAITING
                r.full_clean()
                r.save()

        return super(RentGroupCreate, self).form_valid(form)


class RentEdit(UpdateView, TitleMixin, SubmitBtnMixin, MessageMixin, AdminMixin):

    template_name = "vhmadmin/base_form.html"

    model = VhostRent
    form_class = RentForm
    slug_field = "pk"

    submit_btn_text = _("Save")
    submit_btn_type = "success"

    message = _("Rent modified.")
    message_type = messages.SUCCESS

    def get_success_url(self):
        return reverse("vhmadmin_rent_list")

    def get_title(self):
        rent = self.get_object(self.get_queryset())
        return "%s: %s" % (ugettext("Edit rent"), rent.pk)


class RentChangeStatus(DetailView, AdminMixin):

    model = VhostRent
    slug_field = "pk"

    status = VhostRent.WAITING
    status_current = VhostRent.STATUS_ANY
    message = _("Rent changes done.")
    message_type = messages.SUCCESS

    def get_redirect_url(self):
        return reverse("vhmadmin_rent_list")

    def get(self, request, *args, **kwargs):

        rent = self.get_object(self.get_queryset())

        if not rent.status in self.status_current:
            return HttpResponseNotFound()

        rent.status = self.status
        messages.add_message(request, self.message_type, unicode(self.message))

        rent.save()

        return HttpResponseRedirect(self.get_redirect_url())


class RentCancel(RentChangeStatus):
    status = VhostRent.PASSED
    status_current = VhostRent.STATUS_CURRENT
    message = _("Rent canceled.")


class RentAccept(RentChangeStatus):
    status = VhostRent.WAITING
    status_current = [VhostRent.PENDING]
    message = _("Rent accepted.")


class RentReject(RentChangeStatus):
    status = VhostRent.REJECTED
    status_current = [VhostRent.PENDING]
    message = _("Rent rejected.")


class RentDelete(DeleteView, TitleMixin, AdminMixin):

    template_name = "vhmadmin/rent/delete.html"

    model = VhostRent
    slug_field = "pk"

    def get_title(self):
        rent = self.get_object(self.get_queryset())
        return "%s: %s" % (ugettext("Delete rent"), rent.pk)

    def get_success_url(self):
        return reverse("vhmadmin_rent_list")

    def delete(self, request, *args, **kwargs):
        response = super(RentDelete, self).delete(request, *args, **kwargs)
        if isinstance(response, HttpResponseRedirect):
            messages.add_message(request, messages.SUCCESS, ugettext("Rent deleted."))
        return response