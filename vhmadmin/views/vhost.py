from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import transaction
from django.http.response import HttpResponseRedirect
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.views.generic.list import ListView
from django.utils.translation import ugettext_lazy as _, ugettext
from vhmadmin.forms import VhostForm, VhostGroupForm
from vhmadmin.views import AdminMixin
from vhmdb.models.rent import VhostRent
from vhmdb.models.vhost import Vhost
from vhmweb.requests import get_request
from vhmweb.views import TitleMixin, ListNameMixin, SubmitBtnMixin, MessageMixin


__all__ = (
    "VhostList",
    "VhostDetail",
    "VhostCreate",
    "VhostGroupCreate",
    "VhostEdit",
    "VhostDelete",
)


class VhostList(ListView, TitleMixin, ListNameMixin, AdminMixin):

    title = _("VirtualHosts")
    template_name = "vhmadmin/vhost/list.html"

    model = Vhost
    list_name = "vhosts"

    def get_queryset(self):

        view_rented = get_request().GET.get("view_rented", None)

        if not view_rented:
            q = super(VhostList, self).get_queryset()
        elif view_rented == "1":

            q = "select id, vhost_id from vhm_vhost_rent " \
                "where status=%s group by vhost_id"
            qs = VhostRent.objects.raw(q, params=[VhostRent.ACTIVE])

            q = Vhost.objects.filter(vhostrent__in=qs)

        else:

            q = "select id, vhost_id from vhm_vhost_rent " \
                "where status <> %s group by vhost_id"
            qs = VhostRent.objects.raw(q, params=[VhostRent.ACTIVE])

            q = Vhost.objects.filter(vhostrent__in=qs)

        return q.order_by("name")

    def dispatch(self, request, *args, **kwargs):

        host = request.META.get("HTTP_HOST", None)
        if host:
            self.meta_host = host.split(":")[0]
        else:
            self.meta_host = None

        return super(VhostList, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super(VhostList, self).get_context_data(**kwargs)
        data["host"] = self.meta_host
        return data


class VhostDetail(DetailView, TitleMixin, AdminMixin):

    template_name = "vhmadmin/vhost/detail.html"

    model = Vhost
    context_object_name = "vhost"
    slug_field = "pk"

    def get_title(self):
        vhost = self.get_object(self.get_queryset())
        return "%s: %s" % (ugettext("VirtualHost"), vhost.name)

    def get_context_data(self, **kwargs):
        data = super(VhostDetail, self).get_context_data(**kwargs)

        vhost = data[self.context_object_name]
        data["rents"] = VhostRent.objects.filter(vhost=vhost).order_by("-pk")

        return data


class VhostCreate(CreateView, TitleMixin, SubmitBtnMixin, MessageMixin, AdminMixin):

    title = _("Create VirtualHost")
    template_name = "vhmadmin/base_form.html"

    form_class = VhostForm

    submit_btn_text = _("Create")
    submit_btn_type = "success"

    message = _("VirtualHost created. Note that changes to VirtualHosts and "
                "network interfaces require you to regenerate configurations.")
    message_type = messages.SUCCESS

    def get_success_url(self):
        return reverse("vhmadmin_vhost_list")


class VhostGroupCreate(MessageMixin, TitleMixin, SubmitBtnMixin, AdminMixin):

    title = _("Create numbered VirtualHosts")
    template_name = "vhmadmin/base_form.html"

    form_class = VhostGroupForm

    submit_btn_text = _("Create VirtualHosts")
    submit_btn_type = "success"

    message = _("VirtualHosts created. Note that changes to VirtualHosts and "
                "network interfaces require you to regenerate configurations.")
    message_type = messages.SUCCESS

    def get_success_url(self):
        return reverse("vhmadmin_vhost_list")

    def form_valid(self, form):

        data = form.cleaned_data
        prefix = data["prefix"]
        count = data["count"]
        port_start = data["port_start"]
        options = data["options"]
        allow_override = data["allow_override"]

        with transaction.commit_on_success():
            for i in range(1, count + 1):

                name = "%s%s" % (prefix, i)
                port = port_start + i

                vh = Vhost()
                vh.name = name
                vh.port = port
                vh.options = options
                vh.allow_override = allow_override
                vh.save()

        return super(VhostGroupCreate, self).form_valid(form)


class VhostEdit(UpdateView, TitleMixin, SubmitBtnMixin, MessageMixin, AdminMixin):

    template_name = "vhmadmin/base_form.html"

    model = Vhost
    form_class = VhostForm
    slug_field = "pk"

    submit_btn_text = _("Save")
    submit_btn_type = "success"

    message = _("VirtualHost modified. Note that changes to VirtualHosts and "
                "network interfaces require you to regenerate configurations.")
    message_type = messages.SUCCESS

    def get_title(self):
        vhost = self.get_object(self.get_queryset())
        return "%s: %s" % (ugettext("Edit VirtualHost"), vhost.name)

    def get_success_url(self):
        return reverse("vhmadmin_vhost_list")


class VhostDelete(DeleteView, TitleMixin, AdminMixin):

    template_name = "vhmadmin/vhost/delete.html"

    model = Vhost
    slug_field = "pk"

    def get_title(self):
        vhost = self.get_object(self.get_queryset())
        return "%s: %s" % (ugettext("Delete VirtualHost"), vhost.name)

    def get_success_url(self):
        return reverse("vhmadmin_vhost_list")

    def delete(self, request, *args, **kwargs):
        response = super(VhostDelete, self).delete(request, *args, **kwargs)
        if isinstance(response, HttpResponseRedirect):
            messages.add_message(request, messages.SUCCESS, ugettext("Vhost deleted."))
        return response