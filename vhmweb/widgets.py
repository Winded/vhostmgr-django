from django.forms import widgets
from django.utils.formats import get_format
from django.utils.safestring import mark_safe
from django.utils.translation import get_language


class DateTimeInput(widgets.DateTimeInput):

    def __init__(self, attrs=None, format=None):
        super(DateTimeInput, self).__init__(attrs=attrs, format=format)

    def convert_format(self, format_str):

        format_str = format_str.replace("H", "hh")
        format_str = format_str.replace("Y", "yyyy")

        return format_str

    def render(self, name, value, attrs=None):

        language = get_language()
        dformat = self.convert_format(get_format("DATETIME_FORMAT"))

        itag = super(DateTimeInput, self).render(name, value, attrs=attrs)

        html = "<div id='id_%s_div' class='input-group date'>" % name
        html += itag + \
                "<span class='input-group-addon input-sm'>" \
                "<i class='glyphicon glyphicon-calendar'></i>" \
                "</span>" \
                "</div>"

        html += "<script type='text/javascript'>" \
                "$('#id_%s_div').datetimepicker({" \
                "format: '%s'," \
                "language: '%s'" \
                "});" \
                "</script>" % (name, dformat, language)

        return mark_safe(html)