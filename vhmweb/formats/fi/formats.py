TIME_FORMAT = "H:i"
DATE_FORMAT = "d.m.Y"
DATETIME_FORMAT = "d.m.Y H:i"
DATETIME_INPUT_FORMATS = (
    "%d.%m.%Y %H:%M",
)