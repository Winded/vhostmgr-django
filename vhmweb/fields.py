from django import forms
from vhmweb.widgets import DateTimeInput


class DateTimeField(forms.DateTimeField):

    widget = DateTimeInput