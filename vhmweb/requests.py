"""
Global requests middleware.
"""
from threading import currentThread

_requests = {}


def get_request():
    return _requests.get(currentThread())


class GlobalRequestMiddleware(object):
    def process_request(self, request):
        global _requests
        _requests[currentThread()] = request