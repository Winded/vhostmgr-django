from datetime import timedelta
from django.http.response import HttpResponseRedirect
from vhmdb.models.user import User
from vhmweb import settings


class AuthMiddleware(object):

    def process_request(self, request):

        assert hasattr(request, 'session'), "Session middleware required"

        request.user = None

        user_id = request.session.get("user", None)
        if not user_id:
            return

        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            return

        request.user = user

    def process_template_response(self, request, response):

        if request.user:
            response.context_data["user"] = request.user

        return response


def authenticate(username=None, password=None):

    if not username or not password:
        return None

    try:
        user = User.objects.get(name=username, disabled=False)
    except User.DoesNotExist:
        return None

    if not user.check_password(password):
        return None

    return user


def login(request, user, remember=False):
    request.session["user"] = user.id
    request.user = user
    if remember:
        request.session.set_expiry(timedelta(weeks=1))
    else:
        request.session.set_expiry(0)


def logout(request):

    if not request.user:
        return

    del request.session["user"]
    request.user = None


def login_required(function):

    def f(request, *args, **kwargs):

        if not request.user:

            url = "%s?next=%s" % (settings.LOGIN_URL, request.path)

            return HttpResponseRedirect(url)

        return function(request, *args, **kwargs)

    return f