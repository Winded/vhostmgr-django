from copy import deepcopy
from django.core.urlresolvers import reverse
from django.db.models.query import QuerySet
from django.http.response import HttpResponseRedirect, HttpResponse, HttpResponseNotFound
from django.shortcuts import render_to_response
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _, ugettext
from django.views.decorators.cache import never_cache
from django.views.generic.base import TemplateView, ContextMixin, View, RedirectView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView, CreateView
from django.views.generic.list import ListView
from django.contrib import messages
from vhmdb.models import Vhost
from vhmdb.models.rent import VhostRent
from vhmdb.models.sitepackage import SitePackage
from vhmdb.models.user import User
from vhmweb import forms
from vhmweb.auth import login_required, login, logout
from vhmweb.forms import RentForm, ChangeDisplayNameForm, ChangePasswordForm
from vhmweb.requests import get_request


class TitleMixin(ContextMixin):

    title = "Untitled"

    def get_title(self):
        return self.title

    def get_context_data(self, **kwargs):
        data = super(TitleMixin, self).get_context_data(**kwargs)
        data["title"] = self.get_title()
        return data


class ListNameMixin(ContextMixin):

    list_name = "object_list"

    def get_context_data(self, **kwargs):
        data = super(ListNameMixin, self).get_context_data(**kwargs)
        data[self.list_name] = data.get("object_list", None)
        return data


class NoCacheMixin(View):

    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        return super(NoCacheMixin, self).dispatch(request, *args, **kwargs)


class LoginMixin(NoCacheMixin):

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginMixin, self).dispatch(request, *args, **kwargs)


class SubmitBtnMixin(ContextMixin):
    
    submit_btn_text = "Submit"
    submit_btn_type = "default"
    
    def get_context_data(self, **kwargs):
        data = super(SubmitBtnMixin, self).get_context_data(**kwargs)
        data["submit_btn_text"] = self.submit_btn_text
        data["submit_btn_type"] = self.submit_btn_type
        return data


class MessageMixin(FormView):
    """
    Adds a message on successful form post (when being redirected in POST response).
    """

    message = "You forgot to override the message."
    message_type = messages.INFO

    def form_valid(self, form):

        msg = unicode(self.message)
        messages.add_message(get_request(), message=msg, level=self.message_type)

        return super(MessageMixin, self).form_valid(form)


class Index(TemplateView, LoginMixin, TitleMixin):

    title = _("Front page")

    template_name = "vhmweb/index.html"


class LogIn(FormView, TitleMixin):

    title = _("Login")

    template_name = "vhmweb/login.html"

    form_class = forms.LoginForm

    success_url = "/"

    def get(self, request, *args, **kwargs):

        if request.user:
            return HttpResponseRedirect(reverse("vhmweb_index"))

        return super(LogIn, self).get(request, *args, **kwargs)

    def form_valid(self, form):

        self.success_url = form.cleaned_data["next"]

        login(get_request(), User.objects.get(name=form.cleaned_data["username"]))

        return super(LogIn, self).form_valid(form)


class LogOut(RedirectView, NoCacheMixin):

    def get_redirect_url(self, **kwargs):
        return reverse("vhmweb_login")

    def get(self, request, *args, **kwargs):

        logout(request)

        return super(LogOut, self).get(request, *args, **kwargs)


class ProfileView(TemplateView, TitleMixin, LoginMixin):

    title = _("Profile")
    template_name = "vhmweb/profile/view.html"

    def get_context_data(self, **kwargs):

        user = get_request().user

        data = super(ProfileView, self).get_context_data(**kwargs)

        data["profile_info"] = (
            (ugettext("Username"), user.name),
            (ugettext("Display name"), user.display_name),
            (ugettext("VirtualHost rent limit"), user.get_max_vhosts()),
            (ugettext("Administrator"), ugettext("Yes") if user.admin else ugettext("No")),
        )

        return data


class ProfileEditDisplayName(MessageMixin, SubmitBtnMixin, TitleMixin, LoginMixin):

    title = _("Change display name")
    template_name = "vhmweb/base_form.html"

    form_class = ChangeDisplayNameForm

    message = _("Display name changed.")
    message_type = messages.SUCCESS

    submit_btn_type = "success"
    submit_btn_text = _("Save")

    def get_success_url(self):
        return reverse("vhmweb_profile_view")

    def form_valid(self, form):

        new_dn = form.cleaned_data["display_name"]
        user = get_request().user
        user.display_name = new_dn
        user.save()

        return super(ProfileEditDisplayName, self).form_valid(form)


class ProfileEditPassword(MessageMixin, SubmitBtnMixin, TitleMixin, LoginMixin):

    title = _("Change password")
    template_name = "vhmweb/base_form.html"

    form_class = ChangePasswordForm

    message = _("Password changed.")
    message_type = messages.SUCCESS

    submit_btn_type = "success"
    submit_btn_text = _("Save")

    def get_success_url(self):
        return reverse("vhmweb_profile_view")

    def form_valid(self, form):

        new_password = form.cleaned_data["new_password"]
        user = get_request().user
        user.set_password(new_password)
        user.save()

        return super(ProfileEditPassword, self).form_valid(form)


class RentList(ListView, TitleMixin, ListNameMixin, LoginMixin):

    title = _("My rents")
    template_name = "vhmweb/rent/list.html"

    list_name = "rents"

    def get_queryset(self):

        request = get_request()

        filter = request.GET.get("status", None)
        if filter:
            filter = [filter]
        else:
            filter = VhostRent.STATUS_ANY

        return VhostRent.objects.filter(user=request.user, status__in=filter).order_by("-pk")

    def get_context_data(self, **kwargs):
        data = super(RentList, self).get_context_data(**kwargs)

        status_filters = deepcopy(VhostRent.STATUS_CHOICES)
        data["status_filters"] = status_filters

        return data
    

class RentCreate(CreateView, TitleMixin, SubmitBtnMixin, MessageMixin, LoginMixin):

    title = _("New rent")

    template_name = "vhmweb/base_form.html"
    
    submit_btn_text = _("Rent")
    submit_btn_type = "success"

    message = _("Rent added.")
    message_type = messages.SUCCESS
    
    form_class = RentForm

    def get_success_url(self):
        return reverse("vhmweb_rent_list")

    def get_form_kwargs(self):

        get = get_request().GET

        kwargs = super(RentCreate, self).get_form_kwargs()

        data = kwargs["initial"]
        for k, v in get.iteritems():
            data[k] = v
        data["user"] = get_request().user
        data["status"] = VhostRent.PENDING
        kwargs["initial"] = data

        return kwargs


class RentCancel(TemplateView, TitleMixin, LoginMixin):

    title = _("Cancel rent")

    template_name = "vhmweb/rent/cancel.html"

    def __init__(self, *args, **kwargs):
        super(RentCancel, self).__init__(*args, **kwargs)

    def post(self, request, slug, *args, **kwargs):

        user = request.user

        try:
            rent = VhostRent.objects.get(pk=slug, user=user,
                                         status__in=VhostRent.STATUS_CURRENT)
        except VhostRent.objects.DoesNotExist:
            return HttpResponseNotFound()

        rent.status = VhostRent.PASSED
        rent.end_date = timezone.now()
        rent.save()

        messages.success(request, ugettext("Rent canceled."))

        return HttpResponseRedirect(reverse("vhmweb_rent_list"))


class VhostList(ListView, TitleMixin, ListNameMixin, LoginMixin):

    title = _("VirtualHosts")
    template_name = "vhmweb/vhost/list.html"

    model = Vhost
    list_name = "vhosts"

    def get_queryset(self):

        view_rented = get_request().GET.get("view_rented", None)
        if not view_rented:
            return super(VhostList, self).get_queryset()

        if view_rented == "1":

            q = "select id, vhost_id from vhm_vhost_rent " \
                "where status=%s group by vhost_id"
            qs = VhostRent.objects.raw(q, params=[VhostRent.ACTIVE])

            return Vhost.objects.filter(vhostrent__in=qs)

        else:

            q = "select id, vhost_id from vhm_vhost_rent " \
                "where status <> %s group by vhost_id"
            qs = VhostRent.objects.raw(q, params=[VhostRent.ACTIVE])

            return Vhost.objects.filter(vhostrent__in=qs)

    def dispatch(self, request, *args, **kwargs):

        host = request.META.get("HTTP_HOST", None)
        if host:
            self.meta_host = host.split(":")[0]
        else:
            self.meta_host = None

        return super(VhostList, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super(VhostList, self).get_context_data(**kwargs)
        data["host"] = self.meta_host
        return data


class VhostDetail(DetailView, TitleMixin, LoginMixin):

    template_name = "vhmweb/vhost/detail.html"

    model = Vhost
    context_object_name = "vhost"
    slug_field = "name"

    def get_title(self):
        vhost = self.get_object(self.get_queryset())
        return "%s: %s" % (ugettext("VirtualHost"), vhost.name)


class SitePackageList(ListView, TitleMixin, ListNameMixin, LoginMixin):

    title = _("Site packages")
    template_name = "vhmweb/sitepackage/list.html"

    model = SitePackage
    list_name = "packages"


class SitePackageDetail(DetailView, LoginMixin):

    template_name = "vhmweb/sitepackage/detail.html"

    model = SitePackage
    context_object_name = "package"
    slug_field = "name"


class ErrorBase(TemplateView, TitleMixin):

    title = _("An error occurred")
    description = _("An unknown error has occurred.")
    code = None

    template_name = "vhmweb/errors/base.html"

    def get_context_data(self, **kwargs):
        data = super(ErrorBase, self).get_context_data(**kwargs)
        data["description"] = self.description
        data["code"] = self.code
        return data

    def get(self, request, *args, **kwargs):

        return render_to_response(self.template_name,self.get_context_data())


class Error404(ErrorBase):
    title = _("Page not found")
    description = _("The page you requested cannot be found.")
    code = 404
class Error403(ErrorBase):
    title = _("Access forbidden")
    description = _("You do not have permissions to view this page.")
    code = 403
class Error500(ErrorBase):
    title = _("Server error")
    description = _("A server error has occurred.")
    code = 500