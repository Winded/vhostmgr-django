import os
from django.conf.urls import patterns, include, url
from vhmweb import views, settings


urlpatterns = patterns('',

    url(r"^admin/", include("vhmadmin.urls")),

    url(r"^$", views.Index.as_view(), name="vhmweb_index"),

    url(r"^login/$", views.LogIn.as_view(), name="vhmweb_login"),
    url(r"^logout/$", views.LogOut.as_view(), name="vhmweb_logout"),

    url(r"^profile/$", views.ProfileView.as_view(), name="vhmweb_profile_view"),
    url(r"^profile/edit/display_name$", views.ProfileEditDisplayName.as_view(),
        name="vhmweb_profile_edit_display_name"),
    url(r"^profile/edit/password$", views.ProfileEditPassword.as_view(),
        name="vhmweb_profile_edit_password"),

    url(r"^rents/$", views.RentList.as_view(), name="vhmweb_rent_list"),
    url(r"^rents/new$", views.RentCreate.as_view(), name="vhmweb_rent_create"),
    url(r"^rents/(?P<slug>\d+)/cancel$", views.RentCancel.as_view(), name="vhmweb_rent_cancel"),

    url(r"^vhosts/$", views.VhostList.as_view(), name="vhmweb_vhost_list"),
    # url(r"^vhosts/(?P<slug>.+)$", views.VhostDetail.as_view(), name="vhmweb_vhost_detail"),

    url(r"^packages/$", views.SitePackageList.as_view(), name="vhmweb_sitepackage_list"),
    # url(r"^packages/(?P<slug>.+)$", views.SitePackageDetail.as_view(), name="vhmweb_sitepackage_detail"),

)


handler404 = views.Error404.as_view()
handler403 = views.Error403.as_view()
handler500 = views.Error500.as_view()


if settings.DEBUG:

    docroot = os.path.join(os.path.dirname(__file__), "..", "static")

    urlpatterns += patterns('',
                            url(r"^static/(?P<path>.*)$",
                                "django.views.static.serve",
                                {"document_root": docroot}))