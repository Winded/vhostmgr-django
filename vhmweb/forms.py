from datetime import timedelta
from django import forms
from django.core.exceptions import ValidationError
from django.forms.forms import Form, BaseForm
from django.forms.models import ModelForm
from django.utils.translation import ugettext as _, ugettext_lazy, ugettext
from vhmdb.models.rent import VhostRent
from vhmdb.models.sitepackage import SitePackage
from vhmdb.models.vhost import Vhost
from vhmweb import settings
from vhmweb.auth import authenticate
from vhmweb.fields import DateTimeField
from vhmweb.requests import get_request


class ClassMixin(BaseForm):

    widget_class = "form-control input-sm"

    def __init__(self, *args, **kwargs):

        super(ClassMixin, self).__init__(*args, **kwargs)

        for n, f in self.fields.iteritems():
            f.widget.attrs["class"] = self.widget_class


class LoginForm(Form, ClassMixin):

    username = forms.CharField(label=ugettext_lazy("Username"))
    password = forms.CharField(label=ugettext_lazy("Password"),
                               widget=forms.PasswordInput)

    next = forms.CharField(widget=forms.HiddenInput, required=False)

    def __init__(self, initial=None, *args, **kwargs):

        next = get_request().GET.get("next", "/")
        initial = initial or {}
        initial["next"] = next

        super(LoginForm, self).__init__(initial=initial, *args, **kwargs)

    def clean(self):

        data = super(LoginForm, self).clean()

        username = data.get("username", None)
        password = data.get("password", None)
        user = authenticate(username=username, password=password)

        if not user:
            raise ValidationError(_("Invalid username or password."))

        return data


class RentForm(ModelForm, ClassMixin):

    widget_class = "form-control input-sm"

    vhost = forms.ModelChoiceField(queryset=Vhost.objects, label=ugettext_lazy("VirtualHost"), empty_label=None)

    start_date = DateTimeField(label=ugettext_lazy("Start date"))
    end_date = DateTimeField(label=ugettext_lazy("End date"))

    package = forms.ModelChoiceField(queryset=SitePackage.objects, empty_label=ugettext_lazy("No package"),
                                     label=ugettext_lazy("Package"), required=False)

    class Meta:
        model = VhostRent
        fields = ["user", "vhost", "start_date", "end_date", "package", "status"]
        widgets = {
            "user": forms.HiddenInput(),
            "status": forms.HiddenInput(),
        }

    def __init__(self, initial=None, *args, **kwargs):
        super(RentForm, self).__init__(initial=initial, *args, **kwargs)

    def clean(self):

        data = super(RentForm, self).clean()

        user = get_request().user
        data["user"] = user

        if not user.can_rent():
            raise ValidationError(_("You have reached the maximum amount of current rents."))

        if settings.VHOST_AUTO_APPROVE or user.admin:
            data["status"] = VhostRent.WAITING
        else:
            data["status"] = VhostRent.PENDING

        return data


class ChangeDisplayNameForm(Form, ClassMixin):

    widget_class = "form-control input-sm"

    display_name = forms.CharField(max_length=255, label=_("New name"))

    def __init__(self, initial=None, *args, **kwargs):

        initial = initial or {}
        initial["display_name"] = get_request().user.display_name

        super(ChangeDisplayNameForm, self).__init__(initial=initial, *args, **kwargs)
    

class ChangePasswordForm(Form, ClassMixin):
    
    widget_class = "form-control input-sm"
    
    new_password = forms.CharField(label=_("New password"), widget=forms.PasswordInput)
    new_password_confirm = forms.CharField(label=_("Retype new password"), widget=forms.PasswordInput)
    
    def clean(self):
        
        data = super(ChangePasswordForm, self).clean()

        if self.errors:
            return
        
        if data["new_password"] != data["new_password_confirm"]:
            raise ValidationError(ugettext("Passwords do not match."))

        return data